/************************************************************************/
/* Name     : AsyncClient\DataReceiverImpl.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Sep 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include "Log\SystemLog.h"

#include "MakeClient.h"
#include "DataReceiverImpl.h"

const int EXPIREDTIME = 30;
const int JOBEXPIREDTIME = 1;

struct SendListParams
{
	AsyncClient::TConnectionParams CommentsParams;
	int ExpiredTimeout = 0;
};

struct JobListParams
{
	AsyncClient::TJobConnectionParams JobParams;
};

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& aIo) :
	m_log(NULL),
	r_io(aIo)
{
}

CDataReceiverImpl::~CDataReceiverImpl()
{
}

void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"VXML2AC_sendComment", &CDataReceiverImpl::VXML2AC_sendCommentEventHandler);
	subscribe(L"ANY2AC_asyncHttpRequest", &CDataReceiverImpl::ANY2AC_asyncHttpRequestEventHandler);
	subscribe(L"ANY2AC_getResult", &CDataReceiverImpl::ANY2AC_getResultEventHandler);
}

void CDataReceiverImpl::ConnectHandlers()
{
}

int CDataReceiverImpl::init_impl(CSystemLog* aLog)
{
	if (!aLog)
		return -1;

	m_log = aLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	ConnectHandlers();

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		boost::asio::steady_timer timer(r_io);

		timer.expires_from_now(std::chrono::milliseconds(1000));
		timer.async_wait(yield); //wait for 1 second

		start();
	});

	return 0;
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;
	SendListParams sendParams;

	sendParams.ExpiredTimeout = settings->safe_get_config_int(_T("ClientTimeout"), EXPIREDTIME);
	try
	{
		sendParams.CommentsParams._uri = wtos(settings->get_config_value<std::wstring>(L"CommentsHost"));
		sendParams.CommentsParams._port = wtos(settings->get_config_value<std::wstring>(L"CommentsPort"));
		sendParams.CommentsParams._path = wtos(settings->get_config_value<std::wstring>(L"CommentsPath"));
	}
	catch (core::configuration_error& error)
	{
		m_log->LogString(LEVEL_WARNING, L"configuration_error: ", stow(error.what()).c_str());
		return -1;
	}

	CreateSendTimer(std::move(sendParams));

	JobListParams jobParams;
	jobParams.JobParams._completedHandler = [self = this](Base::JobPtr&& aPtr, std::string aResponse, Base::JobStatus aJobStatus)
	{
		assert(aPtr.get() && "_completedHandler JobPtr empty");

		self->FinishedJob(std::move(aPtr), aResponse, aJobStatus);
	};

	//jobParams.JobParams._timeoutHandler = [self = this](Base::RequestId aRequestId)
	//{
	//	self->FinishedJob(aRequestId);
	//};


	CreateJobTimer(std::move(jobParams));

	return 0;
}

void CDataReceiverImpl::stop()
{
	r_io.stop();
}

void CDataReceiverImpl::on_routerConnect()
{
	if (m_log)
	{
		m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
	}

	set_Subscribes();
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	if (m_log)
	{
		m_log->LogStringModule(LEVEL_FINE, L"Router client", _msg.Dump().c_str());
	}
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	/*if (nStatus == core::ClientStatus::CS_DISCONNECTED)
	{
		try
		{
		               ToDo!
		}
		catch (...)
		{

		}
	}*/
}

/* MAIN CIRCLE */

template <class TReceiver, class TFunction, class TArgs>
static void CreateTimer(
	boost::asio::io_service* aIo,
	CSystemLog * aLog,
	const std::wstring& aTimerName,
	const int aExpiredTimeout, 
	TReceiver* aReceiver,
	TFunction aPredicate, 
	TArgs&& aArgs)
{
	boost::asio::spawn(*aIo,
		[io = aIo,
		log = aLog,
		timerName = aTimerName,
		expiredTimeout = aExpiredTimeout,
		receiver = aReceiver,
		predicate = aPredicate,
		params = std::move(aArgs)](boost::asio::yield_context yield)
	{
		auto LogStringModule = [log = log, timerName = timerName](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
		{
			log->LogStringModule(level, timerName, formatMessage, std::forward<decltype(args)>(args)...);
		};

		try
		{
			boost::asio::steady_timer timer(*io);
			boost::system::error_code errorCode;

			while (io && !(*io).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(expiredTimeout));
				timer.async_wait(yield[errorCode]); //wait for timer

				if (errorCode)
				{
					throw errorCode;
				}

				(receiver->*predicate)(params);
			}
		}
		catch (boost::system::error_code& error)
		{
			if (error != boost::asio::error::eof)
			{
				LogStringModule(LEVEL_WARNING, L"Exception: %s", stow(error.message()));
			}
		}
		catch (std::exception & error)
		{
			LogStringModule(LEVEL_WARNING, L"Exception: %s", stow(error.what()));
		}
		catch (...)
		{
			LogStringModule(LEVEL_WARNING, L"Exception: unhandled exception caught");
		}
	});
}

void CDataReceiverImpl::ProcessSendList(const SendListParams& params)
{
	Base::MessageContainer allJob = mSendDispatcher.GetList();
	if (allJob.empty())
	{
		return;
	}

	AsyncClient::SendMessages(&r_io, params.CommentsParams, std::move(allJob), m_log);
}

void CDataReceiverImpl::ProcessJobList(const JobListParams& params)
{
	NewJobList allJob = mJobDispatcher.GetList();
	if (allJob.empty())
	{
		return;
	}

	for (auto&& job : allJob)
	{
		mJobDispatcher.UpdateJobStatus(job->GetRequestId(), Base::JobStatus::InProgress);
		AsyncClient::MakeRequest(&r_io, params.JobParams, std::move(job), m_log);
	}
}

void CDataReceiverImpl::CreateSendTimer(SendListParams&& aParams)
{
	CreateTimer(
		&this->r_io,
		this->m_log,
		L"SendListTimer",
		aParams.ExpiredTimeout,
		this,
		&CDataReceiverImpl::ProcessSendList,
		std::move(aParams));
}

void CDataReceiverImpl::CreateJobTimer(JobListParams&& aParams)
{
	CreateTimer(
		&this->r_io,
		this->m_log,
		L"JobTimer",
		JOBEXPIREDTIME,
		this,
		&CDataReceiverImpl::ProcessJobList,
		std::move(aParams));
}

void CDataReceiverImpl::FinishedJob(Base::JobPtr&& aJob, std::string aResponse, Base::JobStatus aJobStatus)
{
	aJob->SetResponse(aResponse);
	aJob->SetStatus(aJobStatus);

	auto destination = aJob->GetDestination();
	auto requestId = aJob->GetRequestId();

	if (!mJobDispatcher.UpdateJobStatus(std::move(aJob), aJobStatus))
	{
		m_log->LogString(LEVEL_WARNING, L"ERROR: cannot found and finished job by requestId: %s", wtos(aJob->GetRequestId()).c_str());
	}
	else
	{
		SendAsyncHttpResponse(requestId, destination, aJobStatus, aResponse, std::wstring());
	}
}

void CDataReceiverImpl::MakeJob(
	const Base::RequestId& aRequestId,
	const std::string& aHost,
	const std::string& aPort, 
	const std::string& aRequest,
	int64_t aDestination,
	int aTimeout)
{
	auto job = Base::MakeJob(
		aRequestId,
		aHost,
		aPort,
		aRequest,
		aDestination,
		aTimeout);

	mJobDispatcher.PushToList(std::move(job));
}

/******************************* eof *************************************/