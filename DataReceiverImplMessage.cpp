/************************************************************************/
/* Name     : AsyncClient\DataReceiverImplMessage.cpp                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 24 Sep 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include "DataReceiverImpl.h"

void CDataReceiverImpl::SendAsyncHttpRequestAck(
	const Base::RequestId& aRequestId,
	const Base::DestinationId aDestinationId,
	const std::wstring& aReason,
	bool aIsError)
{
	CMessage msg(L"ANY2AC_asyncHttpRequestAck");
	msg[L"RequestId"] = aRequestId;

	if (aIsError)
	{
		msg[L"Status"] = static_cast<int>(Base::JobStatus::Error);
		msg[L"StatusString"] = Base::StatusToString(Base::JobStatus::Error);
		msg[L"Reason"] = aReason;
	}

	CLIENT_ADDRESS addrTo(aDestinationId);
	this->SendTo(msg, addrTo);
}

void CDataReceiverImpl::SendAsyncHttpResponse(
	const Base::RequestId& aRequestId, 
	const Base::DestinationId aDestinationId, 
	const Base::JobStatus aJobStatus, 
	const std::string& aResponse,
	const std::wstring& aErrorReason)
{
	CMessage msg(L"AC2R_asyncHttpResponse");
	msg[L"Response"] = stow(aResponse);
	msg[L"Status"] = static_cast<int>(aJobStatus);
	msg[L"StatusString"] = Base::StatusToString(aJobStatus);
	msg[L"RequestId"] = aRequestId;

	if (aJobStatus == Base::JobStatus::Error
		&& !aErrorReason.empty())
	{
		msg[L"Reason"] = aErrorReason;
	}

	CLIENT_ADDRESS addrTo(aDestinationId);
	this->SendTo(msg, addrTo);

}

/******************************* eof *************************************/