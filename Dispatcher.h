/************************************************************************/
/* Name     : AsyncClient\Dispatcher.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Oct 2017                                               */
/************************************************************************/
#pragma once
#include "Base.h"
#include "Jobs.h"
#include "DataReceiver.h"

template <class CDispatcherImpl, class TCollectionType>
class CDispatcher
{
protected:
	CDispatcher(CDispatcherImpl* aImpl)
		: pImpl(aImpl)
	{
	}
public:

	typename TCollectionType&& GetList()
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return pImpl->_getList();
	}

	template <class ... TArgs>
	void PushToList(TArgs&&... aArgs)
	{
		std::lock_guard<std::mutex> lock(mMutex);
		pImpl->_pushToList(std::forward<TArgs>(aArgs)...);
	}

private:
	CDispatcherImpl* pImpl = nullptr;
	mutable std::mutex mMutex;
};

class CSendListDispatcher : public CDispatcher <CSendListDispatcher, Base::MessageContainer>
{
	friend class CDispatcher<CSendListDispatcher, Base::MessageContainer>;
public:
	CSendListDispatcher()
		: CDispatcher<CSendListDispatcher, Base::MessageContainer>(this)
	{
	}

private:
	Base::MessageContainer mList;

	Base::MessageContainer&& _getList()
	{
		return std::move(mList);
	}

	template <class ... TArgs>
	void _pushToList(TArgs&&... aArgs)
	{
		mList.push_back(std::forward<TArgs>(aArgs)...);
	}
};

class CJobListDispatcher : public CDispatcher <CJobListDispatcher, NewJobList>
{
	friend class CDispatcher<CJobListDispatcher, NewJobList>;

public:
	CJobListDispatcher()
		: CDispatcher<CJobListDispatcher, NewJobList>(this)
	{
	}

private:
	JobContainer mList;
	NewJobList mNewJob;

	NewJobList&& _getList()
	{
		return std::move(mNewJob);
	}

	void _pushToList(Base::JobPtr&& aPtr)
	{
		bool isNew = mList.emplace(aPtr->GetRequestId(), JobWithStatus(Base::JobStatus::Waiting)).second;
		if (!isNew)
		{
			throw std::runtime_error((boost::format("Job with RequestId = \"%s\" is already existed") % wtos(aPtr->GetRequestId()).c_str()).str());
		}

		mNewJob.push_back(std::move(aPtr));
	}

public:
	//ToDo: maybe make another method to delete a job from the list?
	Base::JobStatus GetStatusAndResponse(const Base::RequestId& aRequestId, bool aIsCancelRequest, std::string& outResponse) /*const*/
	{
		auto status = Base::JobStatus::Unknown;

		std::lock_guard<std::mutex> lock(mMutex);
		const auto cit = mList.find(aRequestId);
		if (cit != mList.cend())
		{
			const JobWithStatus& result = cit->second;
			status = result.JobStatus;
			if (result.JobPtr.get())
			{
				outResponse = result.JobPtr->GetResponse();
			}
			if (aIsCancelRequest && 
				(status == Base::JobStatus::Waiting 
				|| status == Base::JobStatus::Unknown
				|| status == Base::JobStatus::InProgress))
			{
				status = Base::JobStatus::Aborted;
				mList.erase(cit);
			}
		}
		return status;
	}

	bool UpdateJobStatus(const Base::RequestId& aRequestId, Base::JobStatus aJobStatus)
	{
		std::lock_guard<std::mutex> lock(mMutex);

		auto it = mList.find(aRequestId);
		if (it == mList.end())
		{
			throw std::runtime_error("Unknown request id when try to UpdateStatus");
		}
		it->second.JobStatus = aJobStatus;

		return true;
	}

	bool UpdateJobStatus(Base::JobPtr&& aJob, Base::JobStatus aJobStatus)
	{
		std::lock_guard<std::mutex> lock(mMutex);

		auto it = mList.find(aJob->GetRequestId());
		if (it == mList.end())
		{
			return false;
		}
		it->second.JobStatus = aJobStatus;
		it->second.JobPtr = std::move(aJob);

		return true;
	}

private:
	mutable std::mutex mMutex;
};


/******************************* eof *************************************/