// AsyncHttp.h : Declaration of the CAsyncHttp

#pragma once
#include "resource.h"       // main symbols



#include "AsyncClient_i.h"
#include "AsyncBridge.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CAsyncHttp

class ATL_NO_VTABLE CAsyncHttp :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CAsyncHttp, &CLSID_AsyncHttp>,
	public IDispatchImpl<IAsyncHttp, &IID_IAsyncHttp, &LIBID_AsyncClientLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CAsyncHttp()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ASYNCHTTP)

BEGIN_COM_MAP(CAsyncHttp)
	COM_INTERFACE_ENTRY(IAsyncHttp)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(AsyncHttpRequest)(BSTR RequestId, BSTR Body, BSTR ContentType, BSTR Target, BSTR Port, int WaitTimeout, SAFEARRAY **pError);
	STDMETHOD(GetResult)(BSTR RequestId, VARIANT_BOOL IsCancel);
};

OBJECT_ENTRY_AUTO(__uuidof(AsyncHttp), CAsyncHttp)
