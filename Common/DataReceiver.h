/************************************************************************/
/* Name     : MCC\DataReceiver.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/
#pragma once
#include "Router\router_compatibility.h"
#include "Router\system_alarm.h"
#include "Base.h"

class CSystemLog;

template <class Impl>
class CDataReceiver
{
public:
	CDataReceiver()
	{
		CRouterManager::ConnectHandler chandler = boost::bind(&Impl::on_routerConnect, static_cast<Impl*>(this));
		m_router.RegisterConnectHandler(chandler);
		CRouterManager::DeliverErrorHandler ehandler = boost::bind(&Impl::on_routerDeliverError, static_cast<Impl*>(this), _1, _2, _3);
		m_router.RegisterDeliverErrorHandler(ehandler);

		CRouterManager::StatusChangeHandler status_handler = boost::bind(&Impl::on_appStatusChange, static_cast<Impl*>(this), _1, _2);
		m_router.RegisterStatusHandler(status_handler);
	}

	int Init(CSystemLog* pLog)
	{
		return static_cast<Impl*>(this)->init_impl(pLog);
	}

	void Stop()
	{
		return static_cast<Impl*>(this)->stop();
	}

	void SubscribeOnEvents()
	{
		static_cast<Impl*>(this)->set_Subscribes();
	}

	uint32_t GetClientId()const
	{
		//return static_cast<const Impl*>(this)->get_ClientId();
		return m_router.BoundAddress();
	}

	template <class TMessage>
	void SendToAny(TMessage&& message)const
	{
		//static_cast<const Impl*>(this)->sendToAny(std::forward<TMessage>(message));
		m_router.SendToAny(std::forward<TMessage>(message));
	}

	template <class TMessage>
	void SendToAll(TMessage&& message)const
	{
		//static_cast<const Impl*>(this)->sendToAll(std::forward<TMessage>(message));
		m_router.SendToAll(std::forward<TMessage>(message));
	}

	template <class TMessage, class TAddress>
	void SendTo(TMessage&& message, TAddress&& address)const
	{
		//static_cast<const Impl*>(this)->sendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
		m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	}

	template <class TMessage, class TAddress>
	void SendFromTo(TMessage&& _message, TAddress&& _from, TAddress&& _to)const
	{
		m_router.SendFromTo(std::forward<TMessage>(_message), std::forward<TAddress>(_from), std::forward<TAddress>(_to));
	}

	CRouterManager* __getRouter()const { return &m_router; }

	static CDataReceiver<Impl>& Receiver()
	{
		return *this;
	}

protected:
	template<typename Func>
	void subscribe(const std::wstring& _messageName, Func&& func)
	{
		core::SubscribeMessage(m_router, _messageName, static_cast<Impl*>(this), std::forward<Func>(func));
	}

	void onReplyEventHandler2(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
	{

	}

	std::wstring module_name() const noexcept
	{
		return static_cast<const Impl*>(this)->get_ModuleName();
	}

	void activateAlarm(int64_t _errorCode, const std::wstring& errorName, const std::wstring& errorDescription, const std::wstring& _probErrorCause)
	{
		using namespace std::chrono;

		core::temporary_alarm alarm;
		alarm.SetSeverity(core::alarm_severity::Warning);
		alarm.SetAlarmDescription(_errorCode, errorName, errorDescription, _probErrorCause);
		alarm.SetAlarmSource(L"AsyncClient", module_name(), L"Web");
		alarm.SetImportanceTime(minutes(5));
		alarm.Activate(m_router);
	}

private:
	mutable CRouterManager m_router;
};

//class CDataReceiverImpl;


/******************************* eof *************************************/

