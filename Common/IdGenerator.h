/************************************************************************/
/* Name     : MCHAT\IdGenaretor.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Jul 2016                                               */
/************************************************************************/

#pragma once
#include <random>
#include <functional>
#include "Base.h"

#include "Patterns/singleton.h"

using MCCID = unsigned long long;

class CIdGenerator : public singleton <CIdGenerator>
{
	friend class singleton <CIdGenerator>;
private:
	CIdGenerator();

public:
	MCCID makeId();
	std::wstring makeSId();
	MCCID makeId(unsigned int clientId);
	std::wstring makeSId(unsigned int clientId);

private:
	std::random_device _rd;
	std::mt19937_64 _gen;
};

using IdGenerator = singleton_auto_pointer<CIdGenerator>;


/******************************* eof *************************************/