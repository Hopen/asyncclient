#pragma once

namespace utils
{
	template<class value_type, class T>
	std::basic_string<value_type> toStr(T aa)
	{
		std::basic_ostringstream<value_type> out;

		out << aa;
		return out.str();
	}

	template<class T, class string_type>
	T toNum(std::basic_string<string_type> _in)
	{
		T num(0);
		std::basic_istringstream<string_type> inn(_in);
		inn >> num;
		return num;
	}

	template<class T, class string_type>
	T toNumX(std::basic_string<string_type> _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::basic_istringstream<string_type>(_in) >> std::hex >> num;
		return num;
	}
}

