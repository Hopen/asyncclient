/************************************************************************/
/* Name     : AsyncClient\Jobs.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Oct 2017                                               */
/************************************************************************/
#pragma once

#include <unordered_map>
#include <memory>
#include "Base.h"
#include "IdGenerator.h"

namespace Base
{
	class Job
	{
	public:

		Job(const Base::RequestId& aRequestId,
			const std::string& aHost,
			const std::string& aPort,
			const std::string& aRequest,
			const Base::DestinationId aDestinationId,
			const TimerDurationSec aTimeout)
			: mRequestId(aRequestId)
			, mHost(aHost)
			, mPort(aPort)
			, mRequest(aRequest)
			, mStatus(Base::JobStatus::Waiting)
			, mDestinationId(aDestinationId)
			, mTimeoutSec(aTimeout)
		{
		}
		//virtual ~Job() = default;

		Job(const Job&) = default;
		Job(Job&&) = default;

		Job& operator=(const Job&) = default;
		Job& operator=(Job&&) = default;

		auto GetStatus()const noexcept { return mStatus; }
		auto GetRequestId()const noexcept { return mRequestId; }
		auto GetResponse()const noexcept { return mResponse; }
		auto GetDestination()const noexcept { return mDestinationId; }
		auto GetHost()const noexcept { return mHost; }
		auto GetPort()const noexcept { return mPort; }
		auto GetRequest()const noexcept { return mRequest; }
		auto GetTimeout()const noexcept { return mTimeoutSec;  }

		void SetStatus(Base::JobStatus aStatus) { mStatus = aStatus; }
		void SetResponse(std::string aResponce) { mResponse = aResponce; }
	private:
		Base::RequestId mRequestId;
		std::string mHost;
		std::string mPort;
		std::string mRequest;
		std::string mResponse;
		Base::DestinationId mDestinationId;
		TimerDurationSec mTimeoutSec;

		Base::JobStatus mStatus;
	};

	template <class... TArgs>
	JobPtr MakeJob(TArgs&& ... aArgs)
	{
		return std::make_unique<Job>(
			std::forward<TArgs>(aArgs)...);
	}
}


struct JobWithStatus
{
	Base::JobStatus JobStatus;
	Base::JobPtr JobPtr;

	JobWithStatus(Base::JobStatus aJobStatus)
		: JobStatus(aJobStatus)
	{
	}

	JobWithStatus() = default;
};

//static Base::JobPtr CloneJob(const Base::JobPtr& aJob)
//{
//	return std::make_unique<Base::Job>(*aJob);
//}

using JobContainer = std::unordered_map<Base::RequestId, JobWithStatus>;
using NewJobList = std::vector<Base::JobPtr>;

/******************************* eof *************************************/