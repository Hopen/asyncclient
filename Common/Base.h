/************************************************************************/
/* Name     : AsyncClient\Common\Base.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 24 Sep 2017                                               */
/************************************************************************/
#pragma once

#include <boost/format.hpp>
#include "Router\router_compatibility.h"

namespace Base
{
	using ScriptId = uint64_t;
	using RequestId = std::wstring;
	using DestinationId = int64_t;
	using TimerDurationSec = uint16_t;

	using MessageContainer = std::vector<CMessage>;

	using ServerRequestMessageHandler = std::function<std::string(const CMessage& msg)>;

	enum class JobStatus
	{
		Unknown = 0,
		Waiting,
		InProgress,
		Finished,
		Aborted,
		Error
	};

	class Job;
	using JobPtr = std::unique_ptr<Job>;

	const TimerDurationSec REQUESTTIMEOUT = 5;

	static std::wstring StatusToString(JobStatus aJobStatus)
	{
		switch (aJobStatus)
		{
		case JobStatus::Waiting:
			return L"waiting";
		case JobStatus::InProgress:
			return L"inprogress";
		case JobStatus::Finished:
			return L"finished";
		case JobStatus::Aborted:
			return L"aborted";
		case JobStatus::Error:
			return L"error";
		default:
			return L"unknown";
		} 
		return L"unknown";
	}

	static JobStatus StringToStatus(const std::wstring& aStatusString)
	{
		JobStatus result = JobStatus::Unknown;
		if (aStatusString == L"waiting")
		{
			result = JobStatus::Waiting;
		}
		else if (aStatusString == L"inprogress")
		{
			result = JobStatus::InProgress;
		}
		else if (aStatusString == L"finished")
		{
			result = JobStatus::Finished;
		}
		else if (aStatusString == L"aborted")
		{
			result = JobStatus::Aborted;
		}
		else if (aStatusString == L"error")
		{
			result = JobStatus::Error;
		}

		return result;
	}
}

/******************************* eof *************************************/