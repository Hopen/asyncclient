/************************************************************************/
/* Name     : AsyncClient\Common\ThreadHelper.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Sep 2017                                               */
/************************************************************************/
#pragma once
#include <thread>

class CThreadWrapper
{
public:
	CThreadWrapper(std::thread&& _t)
		: t(std::move(_t))
	{

	}
	~CThreadWrapper()
	{
		if (t.joinable())
		{
			t.join();
		}
	}

private:
	std::thread t;
};

/******************************* eof *************************************/