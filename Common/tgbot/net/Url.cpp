/*
 * Copyright (c) 2015 Oleg Morozenkov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "stdafx.h"
#include "tgbot/net/Url.h"

#include "tgbot/tools/StringTools.h"

using namespace std;

std::string QuotedPrintableEncode(const std::string &input)
{
	std::string output;

	char byte;
	const char hex[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	for (std::size_t i = 0; i < input.length(); ++i)
	{
		byte = input[i];

		if ((byte == 0x20) || (byte >= 33) && (byte <= 126) && (byte != 61))
		{
			output += (byte);
		}
		else
		{
			output += ('=');
			output += (hex[((byte >> 4) & 0x0F)]);
			output += (hex[(byte & 0x0F)]);
		}
	}

	return output;
}

std::string QuotedPrintableDecode(const std::string &input)
{
	//                    0  1  2  3  4  5  6  7  8  9  :  ;  <  =  >  ?  @  A   B   C   D   E   F
	const int hexVal[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15 };

	std::string output;

	for (std::size_t i = 0; i < input.length(); ++i)
	{
		if (input.at(i)/*.toAscii()*/ == '=')
		{
			char b1 = input.at(++i) - '0';

			if (b1 < 0 || b1 >= sizeof(hexVal))
			{
				++i;
				continue;
			}

			char a1 = hexVal[b1] << 4;

			char b2 = input.at(++i) - '0';

			if (b2 < 0 || b2 >= sizeof(hexVal))
			{
				continue;
			}

			char a2 = hexVal[b2];

			int b = a1 | a2;
			output += char(b);

			//output+=char(byte(hexVal[input.at(++i)/*.toAscii()*/ - '0'] << 4) | byte(hexVal[input.at(++i)/*.toAscii()*/ - '0']));
		}
		else
		{
			output += (input.at(i)/*.toAscii()*/);
		}
	}

	return output;
}


namespace TgBot {

Url::Url(const string& url) {
	bool isProtocolParsed = false;
	bool isHostParsed = false;
	bool isPathParsed = false;
	bool isQueryParsed = false;

	for (size_t i = 0, count = url.length(); i < count; ++i) {
		char c = url[i];

		if (!isProtocolParsed) {
			if (c == ':') {
				isProtocolParsed = true;
				i += 2;
			} else {
				protocol += c;
			}
		} else if (!isHostParsed) {
			if (c == '/') {
				isHostParsed = true;
				path += '/';
			} else if (c == '?') {
				isHostParsed = isPathParsed = true;
				path += '/';
			} else if (c == '#') {
				isHostParsed = isPathParsed = isQueryParsed = true;
				path += '/';
			} else {
				host += c;
			}
		} else if (!isPathParsed) {
			if (c == '?') {
				isPathParsed = true;
			} else if (c == '#') {
				isPathParsed = isQueryParsed = true;
			} else {
				path += c;
			}
		} else if (!isQueryParsed) {
			if (c == '#') {
				isQueryParsed = true;
			} else {
				query += c;
			}
		} else {
			fragment += c;
		}
	}

	host = StringTools::urlEncode(host, ".");
	path = StringTools::urlEncode(path, "/");
	query = StringTools::urlEncode(query, "&");
	fragment = StringTools::urlEncode(fragment);
	//host = QuotedPrintableEncode(host);
	//path = QuotedPrintableEncode(path);
	//query = QuotedPrintableEncode(query);
	//fragment = QuotedPrintableEncode(fragment);

}

}
