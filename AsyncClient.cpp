// AsyncClient.cpp : Implementation of WinMain

#include "stdafx.h"
#include <comdef.h>

#include "Base.h"

#include "resource.h"
#include "AsyncClient_i.h"
#include "Log/SystemLog.h"
#include "WinAPI/mdump.h"

#include "ThreadHelper.h"
#include "DataReceiverImpl.h"
#include "AsyncBridge.h"

using namespace ATL;

#include <stdio.h>

const wchar_t SERVICE_VERSION[] = L"1.0.0.2";

class CAsyncClientModule : public ATL::CAtlServiceModuleT< CAsyncClientModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_AsyncClientLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ASYNCCLIENT, "{7FBEA9BC-A846-4B4A-853D-AD3D5EC9DE8E}")
		HRESULT InitializeSecurity() throw()
		{
			// TODO : Call CoInitializeSecurity and provide the appropriate security settings for your service
			// Suggested - PKT Level Authentication, 
			// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
			// and an appropriate Non NULL Security Descriptor.

			return S_OK;
		}

		HRESULT Run(int nShowCmd = SW_HIDE) throw()
		{
			::CoInitializeEx(NULL, COINIT_MULTITHREADED);
			singleton_auto_pointer<CSystemLog> log;
			log->LogString(LEVEL_INFO, _T("Starting run"));


			HRESULT hr = S_OK;

			singleton_auto_pointer<MiniDumper> dumper;

			CAsyncClientModule* pT = static_cast<CAsyncClientModule*>(this);

			try
			{
				hr = pT->PreMessageLoop(nShowCmd);
				if (hr != S_OK)
				{
					throw _com_error(hr);
				}

			}
			catch (_com_error& error)
			{
				log->LogString(LEVEL_INFO, _T("Failed to Run service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
			}

			if (hr == S_OK)
			{
				boost::asio::io_service io;
				m_receiver = std::make_unique<CDataReceiverImpl>(io);

				async::singleton_auto_pointer<async::AsyncBridge<CDataReceiverImpl>> receiverAutoPtr(*m_receiver.get());

				if (m_bService && !m_receiver->Init(log->GetInstance()))
				{
					LogEvent(_T("Service started"));
					SetServiceStatus(SERVICE_RUNNING);

					CThreadWrapper t(std::thread(boost::bind(&boost::asio::io_service::run, &io)));

					log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("======================================================="));
					log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("Starting Async Client service %s ..."), SERVICE_VERSION);

					pT->RunMessageLoop();
					m_receiver->Stop();
				}
			}

			if (SUCCEEDED(hr))
			{
				log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("Stopping Async Client service %s ..."), SERVICE_VERSION);
				log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("======================================================="));

				hr = pT->PostMessageLoop();
			}

			::CoUninitialize();
			return hr;
		}

private:
	std::unique_ptr<CDataReceiverImpl> m_receiver;
};

//LPCTSTR c_lpszModuleName = _T("AsyncClient.exe");

CAsyncClientModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	if (_tcsicmp(lpCmdLine, _T("/console")) == 0)
	{
		::CoInitializeEx(NULL, COINIT_MULTITHREADED);
		singleton_auto_pointer<MiniDumper> dumper;

		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, _T("Starting run"));

		boost::asio::io_service io;
		std::unique_ptr<CDataReceiverImpl> receiver = std::make_unique<CDataReceiverImpl>(io);

		if (!receiver->Init(log->GetInstance()))
		{
			CThreadWrapper t(std::thread(boost::bind(&boost::asio::io_service::run, &io)));

			log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("======================================================="));
			log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("Starting Async Client service %s ..."), SERVICE_VERSION);

			MSG msg;
			while (GetMessage(&msg, 0, 0, 0) > 0)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			receiver->Stop();
		}
		::CoUninitialize();
	}


	return _AtlModule.WinMain(nShowCmd);
}

