/************************************************************************/
/* Name     : AsyncClient\Connection.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Oct 2017                                               */
/************************************************************************/

#pragma once
#include <memory>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>


#include "Log/SystemLog.h"
#include "Base.h"

class connection : public std::enable_shared_from_this<connection>
{
public:
	explicit connection(boost::asio::ip::tcp::socket socket, 
		/*ChatId sessionId,*/ 
		CSystemLog* log, 
		Base::ServerRequestMessageHandler handler,
		uint32_t clientId)
		: _socket(std::move(socket))
		,_timer(_socket.get_io_service())
		,_strand(_socket.get_io_service())
		,_log(log)
		,_handler(handler)
		, _clientId(clientId)
	{
		_connection_name = L"connection " + std::to_wstring(++_count);
	}

	~connection()
	{
	}

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, _connection_name, formatMessage, std::forward<TArgs>(args)...);
	}

	void startListening();
private:
	boost::asio::ip::tcp::socket _socket;
	boost::asio::steady_timer _timer;
	boost::asio::io_service::strand _strand;
	CSystemLog* _log;
	Base::ServerRequestMessageHandler _handler;

	static std::atomic<unsigned int> _count;
	std::wstring _connection_name;
	uint32_t _clientId;
};

/******************************* eof *************************************/
