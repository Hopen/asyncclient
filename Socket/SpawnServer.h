/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnServer.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/

#pragma once

#include <string>
#include <functional>
#include "Router/router_compatibility.h"
#include "Base.h"

namespace spawn_server
{
	struct TConnectionServerParams
	{
		std::string _uri;
		std::string _port;
		uint32_t  _clientId;

		Base::ServerRequestMessageHandler _handler;
	};

	bool makeServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog *pLog);
}



/******************************* eof *************************************/