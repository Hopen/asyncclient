/************************************************************************/
/* Name     : AsyncClient\MakeClient.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Oct 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/thread.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>

#include "Log\SystemLog.h"

#include "tgbot/net/httpparser.h"
#include "tgbot/tgtypeparser.h"

#include "MakeClient.h"
#include "Jobs.h"

const int TCP_PACK_SIZE = 1024;
static unsigned int sClientNumber = 0;

namespace AsyncClient
{
	template <class TRequest>
	static std::string _makeRequest(
		const TRequest& aRequest,
		const std::string& aHost,
		const std::string& aPort,
		const std::string& aPath,
		bool aKeepAlive)
	{
		std::string request;
		std::string url = std::string("http://") + aHost + ":" + aPort + "/" + aPath;
		std::vector<TgBot::HttpReqArg> args;

		if (!aRequest.empty())
		{
			args.push_back(TgBot::HttpReqArg("xmldata", aRequest));
		}

		request = TgBot::HttpParser::getInstance().generateRequest(url, args, aKeepAlive);

		return request;
	}

	static std::string _makeRequest(const CMessage& aRequestMsg, const TConnectionParams& aParams, bool aKeepAlive)
	{
		return _makeRequest(aRequestMsg.ToXml(),
			aParams._uri,
			aParams._port,
			aParams._path,
			aKeepAlive);
	}

	static std::string _makeRequest(const Base::JobPtr& aJob)
	{
		return _makeRequest(std::string(),
			aJob->GetHost(),
			aJob->GetPort(),
			aJob->GetRequest(),
			false);
	}

	void SendMessages(
		boost::asio::io_service *aIoService,
		const TConnectionParams& aParams,
		Base::MessageContainer&& aMessageList,
		CSystemLog *aLog)
	{
		if (!aIoService || aIoService->stopped() || aMessageList.empty())
		{
			return;
		}

		boost::asio::spawn(*aIoService,
			[io_service = aIoService,
			params = aParams,
			msg_list = std::move(aMessageList),
			aLog = aLog]
		(boost::asio::yield_context yield)
		{
			auto LogStringModule = [aLog](LogLevel level, const std::wstring& aClientName, const std::wstring& formatMessage, auto&& ...args)
			{
				if (!aLog)
				{
					return;
				}
				aLog->LogStringModule(level, aClientName, formatMessage, std::forward<decltype(args)>(args)...);
			};

			auto read_complete_predicate = [&](int data_len)
			{
				if (!data_len)
					return true;
				return 	data_len < TCP_PACK_SIZE;
			};

			std::wstring logInfoMessage = L"Send client messages";
			LogStringModule(LEVEL_INFO, logInfoMessage, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());

			boost::asio::ip::tcp::resolver resolver(*io_service);
			boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

			boost::asio::ip::tcp::socket _socket(*io_service);

			boost::system::error_code errorCode;

			_socket.async_connect(endpoint, yield[errorCode]);

			if (errorCode)
			{
				throw errorCode;
			}

			auto size = msg_list.size();
			decltype(size) count = 0;

			for (const auto& msg : msg_list)
			{
				auto clientNumber = std::wstring(L"Client ") + std::to_wstring(++sClientNumber);
				try
				{
					std::string request = _makeRequest(msg, params, ++count != size);

					LogStringModule(LEVEL_FINEST, clientNumber, L"Write: %s", stow(request).c_str());
					_socket.async_write_some(boost::asio::buffer(request), yield[errorCode]);
					if (errorCode)
					{
						LogStringModule(LEVEL_INFO, clientNumber, L"errorCode: %s", errorCode.message().c_str());
						throw errorCode;
					}

					std::string in_data;
					int len = 0;

					LogStringModule(LEVEL_INFO, clientNumber, L"Reading...");
					do
					{
						char reply_[TCP_PACK_SIZE] = {};
						_socket.async_read_some(boost::asio::buffer(reply_), yield);
						if (errorCode)
						{
							throw errorCode;
						}

						len = strlen(reply_);

						in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);

					} while (!read_complete_predicate(len));

					LogStringModule(LEVEL_FINEST, clientNumber, L"Read: %s", stow(in_data).c_str());
				}
				catch (boost::system::error_code& error)
				{
					if (error != boost::asio::error::eof)
					{
						LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: %s", stow(error.message()));
					}
				}
				catch (std::runtime_error & error)
				{
					LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: %s", stow(error.what()));
				}
				catch (...)
				{
					LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: unhandled exception caught");
				}
			}
		});
	}

	class AsyncConnection : public std::enable_shared_from_this<AsyncConnection>
	{
	public:
		explicit AsyncConnection(boost::asio::ip::tcp::socket socket,
			Base::JobPtr&& ajob,
			CSystemLog* log,
			AsyncCompletedHandler aCompletedHandler/*,
			AsyncTimeoutHandler aAsyncTimeoutHandler*/)
			: _socket(std::move(socket))
			, _timer(_socket.get_io_service())
			, _strand(_socket.get_io_service())
			, _job(std::move(ajob))
			, _log(log)
			, _completedHandler(aCompletedHandler)
			//, _timeoutHandler(aAsyncTimeoutHandler)
			, _requestId(_job->GetRequestId())
		{
			_connection_name = L"connection " + std::to_wstring(++sClientNumber);
		}

		~AsyncConnection()
		{
		}

		template<typename... TArgs>
		void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
		{
			_log->LogStringModule(level, _connection_name, formatMessage, std::forward<TArgs>(args)...);
		}

		void startTransfering()
		{
			boost::asio::spawn(_strand,
				[self = shared_from_this()](boost::asio::yield_context yield)
			{
				auto read_complete_predicate = [&](int data_len)
				{
					if (!data_len)
						return true;
					return 	data_len < TCP_PACK_SIZE;
				};

				Base::JobStatus result = Base::JobStatus::Unknown;
				std::string response;

				try
				{
					boost::system::error_code errorCode;
					self->_timer.expires_from_now(std::chrono::seconds(3));

					std::string request = _makeRequest(self->_job);

					self->LogStringModule(LEVEL_FINEST, L"Write: %s", stow(request).c_str());
					self->_socket.async_write_some(boost::asio::buffer(request), yield[errorCode]);
					if (errorCode)
					{
						self->LogStringModule(LEVEL_INFO, L"errorCode: %s", errorCode.message().c_str());
						throw errorCode;
					}

					std::string in_data;
					int len = 0;

					self->LogStringModule(LEVEL_INFO, L"Reading...");
					do
					{
						char reply_[TCP_PACK_SIZE] = {};
						self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
						if (errorCode)
						{
							throw errorCode;
						}

						len = strlen(reply_);

						in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);

					} while (!read_complete_predicate(len));

					self->LogStringModule(LEVEL_FINEST, L"Read: %s", stow(in_data).c_str());

					result = Base::JobStatus::Finished;
					response = in_data;
				}
				catch (std::exception& e)
				{
					result = Base::JobStatus::Error;
					response = (boost::format("Exception: %s") % e.what()).str();
					self->LogStringModule(LEVEL_FINEST, stow(response).c_str());
					self->_socket.close();
					self->_timer.cancel();
				}
				catch (...)
				{
					result = Base::JobStatus::Error;
					response = "Unknown exception caught";

					self->LogStringModule(LEVEL_WARNING, L"Connection failed with error: unhandled exception caught");
					self->_socket.close();
					self->_timer.cancel();
				}

				if (self->_job.get())
				{
					self->_completedHandler(std::move(self->_job), response, result);
				}

			});

			boost::asio::spawn(_strand,
				[self = shared_from_this()](boost::asio::yield_context yield)
			{
				while (self->_socket.is_open())
				{
					boost::system::error_code ignored_ec;
					self->_timer.async_wait(yield[ignored_ec]);
					if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
					{
						self->LogStringModule(LEVEL_FINEST, L"Socket timeout");
						if (self->_socket.is_open())
						{
							self->_socket.close();
							self->LogStringModule(LEVEL_FINEST, L"Close socket by timeout");
						}
					}
				}

				if (self->_job.get())
				{
					self->_completedHandler(std::move(self->_job), "Connection aborted by timeout", Base::JobStatus::Aborted);
				}
			});
		}

	private:
		boost::asio::ip::tcp::socket _socket;
		boost::asio::steady_timer _timer;
		boost::asio::io_service::strand _strand;
		Base::JobPtr _job;
		CSystemLog* _log;
		AsyncCompletedHandler _completedHandler;
		//AsyncTimeoutHandler _timeoutHandler;
		Base::RequestId _requestId;

		static std::atomic<unsigned int> _count;
		std::wstring _connection_name;
	};


	void MakeRequest(
		boost::asio::io_service *aIoService,
		const TJobConnectionParams& aParams,
		Base::JobPtr&& ajob,
		CSystemLog *aLog)
	{
		if (!aIoService || aIoService->stopped())
		{
			return;
		}

		boost::asio::spawn(*aIoService,
			[io_service = aIoService,
			params = aParams,
			aLog = aLog,
			job = std::move(ajob)]
		(boost::asio::yield_context yield)
		{
			auto LogStringModule = [aLog](LogLevel level, const std::wstring& aClientName, const std::wstring& formatMessage, auto&& ...args)
			{
				if (!aLog)
				{
					return;
				}
				aLog->LogStringModule(level, aClientName, formatMessage, std::forward<decltype(args)>(args)...);
			};

			auto read_complete_predicate = [&](int data_len)
			{
				if (!data_len)
					return true;
				return 	data_len < TCP_PACK_SIZE;
			};

			std::wstring logInfoMessage = L"Make request";

			try
			{
				auto uri = job->GetHost();
				auto port = job->GetPort();

				LogStringModule(LEVEL_INFO, logInfoMessage, L"Connecting to: %s:%s", stow(uri).c_str(), stow(port).c_str());

				boost::asio::ip::tcp::resolver resolver(*io_service);
				boost::asio::ip::tcp::resolver::query query(uri, port);
				boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
				boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

				boost::asio::ip::tcp::socket _socket(*io_service);

				boost::system::error_code errorCode;

				_socket.async_connect(endpoint, yield[errorCode]);

				if (errorCode)
				{
					throw errorCode;
				}

				std::make_shared<AsyncConnection>(
					std::move(_socket), 
					std::move(std::remove_const_t<Base::JobPtr&>(job)),
					aLog, 
					params._completedHandler/*,
					params._timeoutHandler*/)->startTransfering();
			}
			catch (std::exception& e)
			{
				LogStringModule(LEVEL_FINEST, logInfoMessage, L"Exception: %s", stow(e.what()).c_str());
			}
			catch (...)
			{
				LogStringModule(LEVEL_WARNING, logInfoMessage, L"Connection failed with error: unhandled exception caught");
			}
		});
	}

} //End namespace



  /******************************* eof *************************************/