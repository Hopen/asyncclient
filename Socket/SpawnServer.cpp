/************************************************************************/
/* Name     : MVEON\Common\Socket\SpawnServer.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Jan 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include "SpawnServer.h"
#include "connection.h"

namespace spawn_server
{
	bool makeServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog * pLog)
	{

		boost::asio::spawn(*io_service, [/*&LogStringModule,*/ io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
		{
			auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
			{
				pLog->LogStringModule(level, L"Server", formatMessage, std::forward<decltype(args)>(args)...);
			};


			try
			{
				LogStringModule(LEVEL_FINEST, L"Open socket...");

				boost::asio::ip::tcp::resolver resolver(*io_service);
				boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
				boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
				boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

				LogStringModule(LEVEL_FINEST, L"Add acceptor...");
				/// Acceptor used to listen for incoming connections.
				boost::asio::ip::tcp::acceptor acceptor_(*io_service);
				acceptor_.open(endpoint.protocol());
				acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
				acceptor_.bind(endpoint);
				acceptor_.listen();

				boost::system::error_code errorCode;

				//IdGenerator generator;

				LogStringModule(LEVEL_INFO, L"Waiting for connection...");


				while (!(*io_service).stopped())
				{
					boost::asio::ip::tcp::socket _socket(*io_service);

					acceptor_.async_accept(_socket, yield[errorCode]);

					if (errorCode)
						throw errorCode;

					std::make_shared<connection>(std::move(_socket), pLog, params._handler, params._clientId)->startListening();
				}
			}
			catch (boost::system::error_code& error)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
			}
			catch (...)
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
			}

		});

		int test = 0;
		return false;
	}

}


/******************************* eof *************************************/