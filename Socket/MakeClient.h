/************************************************************************/
/* Name     : AsyncClient\MakeClient.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Oct 2017                                               */
/************************************************************************/

#pragma once
#include "Base.h"

class CSystemLog;

namespace boost
{
	namespace asio
	{
		class io_service;
	}
}

namespace AsyncClient
{
	using AsyncCompletedHandler = std::function<void(Base::JobPtr&& aPtr, std::string aResponse, Base::JobStatus aJobStatus)>;
	using AsyncTimeoutHandler = std::function<void(const Base::RequestId& aRequestId)>;

	struct TConnectionParams
	{
		std::string _uri;
		std::string _port;
		std::string _path;
		std::string _request;
		std::string _response;
	};

	struct TJobConnectionParams
	{
		AsyncCompletedHandler _completedHandler;
		//AsyncTimeoutHandler _timeoutHandler;
	};

	void SendMessages(
		boost::asio::io_service *aIoService,
		const TConnectionParams& aParams,
		Base::MessageContainer&& aMessageList,
		CSystemLog *aLog);

	void MakeRequest(
		boost::asio::io_service *aIoService,
		const TJobConnectionParams& aParams,
		Base::JobPtr&& ajob,
		CSystemLog *aLog);
}


/******************************* eof *************************************/