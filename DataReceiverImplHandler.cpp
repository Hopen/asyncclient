/************************************************************************/
/* Name     : AsyncClient\DataReceiverImplHandler.cpp                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 24 Sep 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include "DataReceiverImpl.h"
#include "utils.h"

void CDataReceiverImpl::VXML2AC_sendCommentEventHandler(const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	//m_log->LogStringModule(LEVEL_INFO, L"[IN]", _msg.Dump());
	mSendDispatcher.PushToList(_msg);
}

void CDataReceiverImpl::ANY2AC_asyncHttpRequestEventHandler(const CMessage & _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	//m_log->LogStringModule(LEVEL_INFO, L"[IN]", _msg.Dump());

	auto destination = _from.AsMessageParamterInt();
	bool isError = false;
	std::wstring errorString;
	Base::RequestId requestId;

	try
	{
		_msg.CheckParam(L"RequestId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"Host", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"Port", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"Request", CMessage::CheckedType::String, CMessage::ParamType::Optional);

		requestId = _msg.SafeReadParam(L"RequestId", CMessage::CheckedType::String, L"").AsWideStr();

		MakeJob(
			requestId,
			_msg.SafeReadParam(L"Host", CMessage::CheckedType::String, L"").AsStr(),
			_msg.SafeReadParam(L"Port", CMessage::CheckedType::String, L"").AsStr(),
			_msg.SafeReadParam(L"Request", CMessage::CheckedType::String, L"").AsStr(),
			destination,
			_msg.SafeReadParam(L"Timeout", CMessage::CheckedType::Int, Base::REQUESTTIMEOUT).AsInt());
	}
	catch (const std::exception& e)
	{
		isError = true;
		errorString = stow(e.what());
		m_log->LogStringModule(LEVEL_WARNING, L"CDataReceiverImpl", L"Exception: %s", errorString.c_str());
	}
	catch (...)
	{
		isError = true;
		errorString = L"Unknown exception";
		m_log->LogStringModule(LEVEL_WARNING, L"CDataReceiverImpl", L"Exception: %s", errorString.c_str());
	}

	SendAsyncHttpRequestAck(requestId, destination, errorString, isError);
}

void CDataReceiverImpl::ANY2AC_getResultEventHandler(const CMessage & _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	//m_log->LogStringModule(LEVEL_INFO, L"[IN]", _msg.Dump());

	Base::RequestId requestId;
	auto destination = _from.AsMessageParamterInt();
	std::string response;
	bool isCancelRequest = false;
	std::wstring errorReason;

	auto status = Base::JobStatus::Error;

	try
	{
		_msg.CheckParam(L"RequestId", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);

		requestId = _msg.SafeReadParam(L"RequestId", CMessage::CheckedType::String, L"").AsWideStr();
		
		isCancelRequest = _msg.SafeReadParam(L"CancelRequest", CMessage::CheckedType::Bool, false).AsBool();

		status = mJobDispatcher.GetStatusAndResponse(
			requestId,
			isCancelRequest,
			response);
	}
	catch (const std::exception& e)
	{
		errorReason = stow(e.what());
		m_log->LogStringModule(LEVEL_WARNING, L"CDataReceiverImpl", L"Exception: %s", errorReason.c_str());
	}
	catch (...)
	{
		errorReason = L"Unknown exception";
		m_log->LogStringModule(LEVEL_WARNING, L"CDataReceiverImpl", errorReason);
	}

	SendAsyncHttpResponse(requestId, destination, status, response, errorReason);

}


/******************************* eof *************************************/