// AsyncHttp.cpp : Implementation of CAsyncHttp

#include "stdafx.h"
#include "AsyncHttp.h"
#include "DataReceiverImpl.h"

// CAsyncHttp

static void MakeComError(const std::string& aErrorStr, SAFEARRAY **outError)
{
	*outError = ::SafeArrayCreateVector(VT_I2, 0, aErrorStr.size());
	if (*outError != NULL)
	{
		BYTE* psadata = NULL;
		HRESULT hr = ::SafeArrayAccessData(*outError, (void**)&psadata);
		if (SUCCEEDED(hr))
		{
			memcpy(psadata, aErrorStr.c_str(), aErrorStr.size());
			hr = ::SafeArrayUnaccessData(*outError);
		}
	}
}

static std::wstring CheckParam(wchar_t* aRequest, char* aCheckedName)
{
	std::wstring checked(aRequest);
	if (checked.size() == 0)
	{
		throw std::runtime_error(std::string("Empty #") + std::string(aCheckedName) + " param");
	}

	return checked;
}

STDMETHODIMP CAsyncHttp::AsyncHttpRequest(
	BSTR RequestId, 
	BSTR Body, 
	BSTR ContentType, 
	BSTR Target, 
	BSTR Port, 
	int WaitTimeout,
	SAFEARRAY **pError)
{
	singleton_auto_pointer<CSystemLog> log;

	HRESULT result = S_OK;

	/*
		const Base::RequestId& aRequestId,
	const std::string& aHost,
	const std::string& aPort, 
	const std::string& aRequest,
	int64_t aDestination,
	int aTimeout)

	*/

	try
	{
		async::singleton_auto_pointer<async::AsyncBridge<CDataReceiverImpl>> receiverAutoPtr;

		receiverAutoPtr->GetImpl().MakeJob(
			CheckParam(RequestId, "RequestId"),
			wtos(CheckParam(Target, "Target")),
			wtos(CheckParam(Port, "Port")),
			wtos(std::wstring(Body)),
			0,
			WaitTimeout);
	}
	catch (const std::exception& aError)
	{
		result = E_FAIL;

		std::string errorStr = aError.what();
		log->LogString(LEVEL_WARNING, L"Exception (AsyncHttpRequest): %s", stow(errorStr).c_str());

		MakeComError(errorStr, pError);
	}

	return result;
}


STDMETHODIMP CAsyncHttp::GetResult(BSTR RequestId, VARIANT_BOOL IsCancel)
{
	// TODO: Add your implementation code here

	return S_OK;
}
