/************************************************************************/
/* Name     : MVEON_CLIENT\MyRouterClient.h                             */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 05 Feb 2017                                               */
/************************************************************************/

#pragma once
#include "AllHeaders.h"
#include "SpawnServer.h"

class CMyRouterClient final
{
private:
	void _handler(const CMessage& msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
	{
		auto messageName = msg.GetName();
		std::lock_guard<std::mutex> lock(m_mutex);

		//auto& it = m_subscriptions.find(messageName);
		auto it = std::find_if(m_subscriptions.begin(), m_subscriptions.end(), [messageName](const auto& pair)
		{
			return pair.first == messageName && pair.second.second == false;
		});

		if (m_subscriptions.find(messageName) == m_subscriptions.end())
		{
			throw (std::runtime_error((boost::format("Subscription for \"%s\" not exist!") % wtos(messageName)).str()));
		}

		it->second.first(msg, _from, _to);
		it->second.second = true;
	}

	void _reset()
	{
		m_subscriptions.clear();
	}

public:

	CMyRouterClient(unsigned int timeout = SAVE_TIME_OUT)
		:m_saveTime(timeout)
	{
		//wait for router connected
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	};

	uint32_t GetClientId()const noexcept
	{
		return m_router.BoundAddress();
	}

	//~CMyRouterClient()
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);
	//	auto cit = std::find_if(m_subscriptions.cbegin(), m_subscriptions.cend(), [](const auto& subscription)
	//	{
	//		return subscription.second.second == false;
	//	});
	//	BOOST_CHECK(cit == m_subscriptions.cend());
	//}

	void Expect(const std::wstring& _messageName, ChatTextMessageHandler_test _handler)
	{
		//if (m_subscriptions.find(_messageName) != m_subscriptions.cend())
		//{
		//	throw (std::runtime_error((boost::format("Already have subscription for: \"%s\"") % wtos(_messageName)).str()));
		//}
		//m_subscriptions[_messageName] = std::pair<ChatTextMessageHandler_test, bool>(_handler, false);

		m_subscriptions.emplace(_messageName, std::pair<ChatTextMessageHandler_test, bool>(_handler, false));
		core::SubscribeMessage(m_router, _messageName, static_cast<CMyRouterClient*>(this), &CMyRouterClient::_handler);
	}

private:
	using MessageSend = std::function<void(boost::asio::io_service& io, const CMessage& msg)>;

	bool start(Base::MessageContainer&& messageContainer, CSystemLog *pLog, MessageSend send)
	{
		bool result = true;
		boost::coroutines::asymmetric_coroutine<void>::push_type waiter(
			[&, self = this](boost::coroutines::asymmetric_coroutine<void>::pull_type& in)
		{
			boost::asio::io_service io;
			boost::asio::spawn(io, [messageList = std::move(messageContainer), self = self, &io, &result, &send](boost::asio::yield_context yield)
			{
				for (const auto& msg : messageList)
				{
					send(io, msg);
				}

				boost::asio::steady_timer timer(io);

				if (!self->m_subscriptions.size())
				{
					return;
				}

				bool all_message_come = false;
				unsigned int counter = 0;
				while (!all_message_come)
				{
					timer.expires_from_now(std::chrono::seconds(1));
					timer.async_wait(yield); //wait for 1 seconds
					++counter;
					{
						std::lock_guard<std::mutex> lock(self->m_mutex);
						auto cit = std::find_if(self->m_subscriptions.cbegin(), self->m_subscriptions.cend(), [](const auto& subscription)
						{
							return subscription.second.second == false;
						});
						all_message_come = cit == self->m_subscriptions.cend();
					}

					if (counter > self->m_saveTime)
					{
						result = false;
						break;
					}
				}
				io.stop();
			});
			io.run();
		});

		waiter();

		_reset();
		return result;
	}
public:

	//bool Start(MessageContainer&& messageContainer, const ssl_veon_client::TConnectionParams& params, CSystemLog *pLog)
	//{
	//	return start(std::move(messageContainer), pLog, [self = this, &params, pLog](boost::asio::io_service& io, const CMessage& msg)
	//	{
	//		std::wstring request = CMessageParser::ToStringUnicode(msg);

	//		ssl_veon_client::TConnectionParams newParams(params);
	//		newParams._request = wtos(request);
	//		ssl_veon_client::MakeClient(&io, std::move(newParams), pLog);
	//	});
	//}

	bool Start(Base::MessageContainer&& messageContainer, spawn_server::TConnectionServerParams&& params, CSystemLog *pLog)
	{
		return start(std::move(messageContainer), pLog, 
			[self = this, params = std::move(params), log = pLog](boost::asio::io_service& io, const CMessage& msg)
		{
			spawn_server::makeServer(&io, std::move(params), log->GetInstance());

			self->m_router.SendToAny(msg);
		});
	}

	bool Start(Base::MessageContainer&& messageContainer, CSystemLog *pLog)
	{
		return start(std::move(messageContainer), pLog, [self = this](boost::asio::io_service& /*io*/, const CMessage& msg)
		{
			self->m_router.SendToAny(msg);
		});
	}

	bool Start(Base::MessageContainer&& messageContainer, CLIENT_ADDRESS addr_to, CSystemLog *pLog)
	{
		return start(std::move(messageContainer), pLog, [self = this, addr_to = addr_to](boost::asio::io_service& /*io*/, const CMessage& msg)
		{
			self->m_router.SendTo(msg, addr_to);
		});
	}

	void Stop()
	{

	}

private:
	CRouterManager m_router;

	//using SubscriptionContainer = std::map<std::wstring, std::pair<ChatTextMessageHandler_test, bool>>;
	using SubscriptionContainer = std::multimap<std::wstring, std::pair<ChatTextMessageHandler_test, bool>>;

	SubscriptionContainer m_subscriptions;
	unsigned int m_saveTime{ 0 };
	mutable std::mutex m_mutex;
	spawn_server::TConnectionServerParams m_serverParams;
	bool m_bMakeServer = false;
};

/******************************* eof *************************************/