/************************************************************************/
/* Name     : AsyncClient\AsyncHttpRequest.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 07 Oct 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <chrono>
#include <thread>
#include <boost/test/unit_test.hpp>
#include "IdGenerator.h"
#include "WinAPI/ProcessHelper.h"
#include "myrouterclient.h"
#include "utils.h"

using namespace std::chrono_literals;

BOOST_AUTO_TEST_SUITE(async_errors_tests)

BOOST_AUTO_TEST_CASE(Process_ANY2AC_acyncHttpRequest_NotFoundRequestId)
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Process_ANY2AC_acyncHttpRequest_NotFoundRequestId");

	SystemConfig settings;

	std::wstring serverHost = settings->safe_get_config_value(L"ServerHost", std::wstring(L"127.0.0.1"));
	std::wstring serverPort = settings->safe_get_config_value(L"ServerPort", std::wstring(L"8080"));
	auto ScriptId = CIdGenerator::GetInstance()->makeId();
	std::wstring requestString = L"Testing/SaveLog";

	spawn_server::TConnectionServerParams params;
	params._uri = wtos(serverHost);
	params._port = wtos(serverPort);

	params._handler = [&](const CMessage& _msg)
	{
		return std::string();
	};

	CMyRouterClient router_client;
	Base::RequestId expactedRequestId = CIdGenerator::GetInstance()->makeSId();

	CMessage msg(L"ANY2AC_asyncHttpRequest");
	// not request id !!!  msg[L"RequestId"] = utils::toStr<wchar_t>(expactedRequestId);
	msg[L"ScriptId"] = ScriptId;
	msg[L"Host"] = serverHost;
	msg[L"Port"] = serverPort;
	msg[L"Request"] = requestString;

	Base::MessageContainer msgList = { msg };

	router_client.Expect(L"ANY2AC_asyncHttpRequestAck",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"ANY2AC_asyncHttpRequestAck");
		auto requestId = _msg[L"RequestId"].AsWideStr();

		BOOST_CHECK(_msg[L"Status"].AsInt() == static_cast<int>(Base::JobStatus::Error));
		BOOST_CHECK(_msg[L"StatusString"].AsWideStr() == Base::StatusToString(Base::JobStatus::Error));

		BOOST_CHECK(_msg[L"Reason"].AsWideStr() == L"Parameter [RequestId]. Parameter doesn't exist");
		
		BOOST_CHECK(requestId == L"");
	});

	router_client.Start(std::move(msgList), std::move(params), log->GetInstance());

	log->LogString(LEVEL_INFO, L"Done");
}

BOOST_AUTO_TEST_CASE(Process_ANY2AC_getResult_NotFoundRequestId)
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Process_ANY2AC_getResult_NotFoundRequestId");

	SystemConfig settings;

	std::wstring serverHost = settings->safe_get_config_value(L"ServerHost", std::wstring(L"127.0.0.1"));
	std::wstring serverPort = settings->safe_get_config_value(L"ServerPort", std::wstring(L"8080"));
	auto ScriptId = CIdGenerator::GetInstance()->makeId();
	std::string responseString = "some data";
	std::wstring requestString = L"Testing/SaveLog";

	spawn_server::TConnectionServerParams params;
	params._uri = wtos(serverHost);
	params._port = wtos(serverPort);

	params._handler = [&](const CMessage& _msg)
	{
		log->LogString(LEVEL_INFO, L"Dump request: %s", _msg.Dump());

		return responseString;
	};

	CMyRouterClient router_client;
	Base::RequestId expactedRequestId = CIdGenerator::GetInstance()->makeSId();

	CMessage msg(L"ANY2AC_asyncHttpRequest");
	msg[L"RequestId"] = expactedRequestId;
	msg[L"ScriptId"] = ScriptId;
	msg[L"Host"] = serverHost;
	msg[L"Port"] = serverPort;
	msg[L"Request"] = requestString;

	Base::MessageContainer msgList = { msg };

	router_client.Expect(L"AC2R_asyncHttpResponse",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"AC2R_asyncHttpResponse");
		BOOST_CHECK(_msg[L"Response"].AsStr() == responseString);
		BOOST_CHECK(_msg[L"Status"].AsInt() == static_cast<int>(Base::JobStatus::Finished));
		BOOST_CHECK(_msg[L"StatusString"].AsWideStr() == Base::StatusToString(Base::JobStatus::Finished));
	});

	router_client.Expect(L"ANY2AC_asyncHttpRequestAck",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"ANY2AC_asyncHttpRequestAck");
		auto requestId = _msg[L"RequestId"].AsWideStr();
		BOOST_CHECK(expactedRequestId == requestId);
	});

	router_client.Start(std::move(msgList), std::move(params), log->GetInstance());

	CMessage getResultMsg(L"ANY2AC_getResult");
	// no request id getResultMsg[L"RequestId"] = utils::toStr<wchar_t>(expactedRequestId);
	getResultMsg[L"CancelRequest"] = false;

	msgList = { getResultMsg };

	router_client.Expect(L"AC2R_asyncHttpResponse",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"AC2R_asyncHttpResponse");
		auto requestId = _msg[L"RequestId"].AsWideStr();

		BOOST_CHECK(_msg[L"Status"].AsInt() == static_cast<int>(Base::JobStatus::Error));
		BOOST_CHECK(_msg[L"StatusString"].AsWideStr() == Base::StatusToString(Base::JobStatus::Error));

		BOOST_CHECK(_msg[L"Reason"].AsWideStr() == L"Parameter [RequestId]. Parameter doesn't exist");

		BOOST_CHECK(requestId == L"");
	});

	router_client.Start(std::move(msgList), std::move(params), log->GetInstance());

	log->LogString(LEVEL_INFO, L"Done");
}

BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/