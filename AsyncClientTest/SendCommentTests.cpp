/************************************************************************/
/* Name     : AsyncClient\SendCommentTests.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Oct 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "SpawnServer.h"
#include "WinAPI/ProcessHelper.h"
//#include "codec.h"

BOOST_AUTO_TEST_SUITE(sendcomments_tests)

//BOOST_AUTO_TEST_CASE(VXML2AC_sendComment)
//{
//	singleton_auto_pointer<CSystemLog> log;
//	log->LogString(LEVEL_INFO, L"VXML2AC_sendComment send");
//
//	SystemConfig settings;
//
//	spawn_server::TConnectionServerParams params;
//	params._uri = wtos(settings->safe_get_config_value(L"CommentsHost", std::wstring(L"0.0.0.0")));
//	params._port = wtos(settings->safe_get_config_value(L"CommentsPort", std::wstring(L"80")));
//
//	CRouterManager m_router;
//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//	auto clientId = m_router.BoundAddress();
//
//	CMessage send_msg(L"VXML2AC_sendComment");
//	send_msg[L"ScriptId"] = L"123";
//	send_msg[L"Comment"] = L"This is Comment!!";
//
//	boost::asio::io_service io;
//	params._handler = [&](const CMessage& _msg)
//	{
//		BOOST_CHECK(_msg[L"ScriptId"].AsWideStr() == send_msg[L"ScriptId"].AsWideStr());
//		BOOST_CHECK(_msg[L"Comment"].AsWideStr() == send_msg[L"Comment"].AsWideStr());
//
//		//io.stop();
//
//		return std::string("201");
//	};
//
//	spawn_server::makeServer(&io, std::move(params), log->GetInstance());
//	io.post([&]()
//	{
//		CLIENT_ADDRESS addr_to(CLIENT_ADDRESS::Any());
//
//		m_router.SendFromTo(send_msg, clientId, addr_to);
//	});
//
//	io.run();
//}

BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/