/************************************************************************/
/* Name     : AsyncClient\AsyncBridge.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 18 Sep 2018                                               */
/************************************************************************/
#pragma once
#include <exception>
#include <boost/noncopyable.hpp>

namespace async
{
	template <class T>
	class singleton : public boost::noncopyable
	{
		static T *		m_instance;
		static LONG		m_dwCounter;
	public:

		template <class... TArgs>
		static typename std::enable_if<(sizeof...(TArgs) == 0), T*>::type GetInstance(TArgs&&... aArgs)
		{
			if (m_instance == nullptr)
			{
				throw std::runtime_error("No async singleton instance has created");
			}

			::InterlockedIncrement(&m_dwCounter);
			return m_instance;
		}

		template <class... TArgs>
		static typename std::enable_if<(sizeof...(TArgs) > 0), T*>::type GetInstance(TArgs&&... aArgs) 
		{
			if (m_instance == nullptr)
			{
				m_instance = new T(std::forward<TArgs>(aArgs)...);
			}

			::InterlockedIncrement(&m_dwCounter);
			return m_instance;
		}

		static DWORD ReleaseInstance()
		{
			::InterlockedDecrement(&m_dwCounter);
			if (m_dwCounter <= 0 && m_instance != nullptr)
			{
				delete m_instance;
				m_instance = nullptr;
			}
			return m_dwCounter;
		}
	};

	template <class T>
	LONG singleton<T>::m_dwCounter = 0;

	template <class T>
	T * singleton<T>::m_instance = nullptr;

	template <class T>
	class singleton_auto_pointer
	{
		T * m_instance;
	public:
		template <class... TArgs>
		singleton_auto_pointer(TArgs&&... aArgs)
		{
			m_instance = T::GetInstance(std::forward<TArgs>(aArgs)...);
		}

		~singleton_auto_pointer()
		{
			T::ReleaseInstance();
		}

		T * operator -> ()
		{
			return m_instance;
		}
	};

	template <class TImpl>
	class AsyncBridge : public singleton<AsyncBridge<TImpl>>
	{
	public:
		AsyncBridge() = delete;

		AsyncBridge(TImpl& aImpl)
			: mImpl(aImpl)
		{}

		TImpl& GetImpl()
		{
			return mImpl;
		}

	private:
		TImpl& mImpl;
	};
}



/******************************* eof *************************************/