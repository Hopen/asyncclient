/************************************************************************/
/* Name     : AsyncClient\DataReceiverImpl.h                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : AsyncClient                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Sep 2017                                               */
/************************************************************************/
#pragma once

#include "Router\router_compatibility.h"
#include "Base.h"
#include "DataReceiver.h"
#include "Dispatcher.h"

struct SendListParams;
struct JobListParams;

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();


	void MakeJob(
		const Base::RequestId& aRequestId,
		const std::string& aHost,
		const std::string& aPort,
		const std::string& aRequest,
		int64_t aDestination,
		int aTimeout);

private:
	int init_impl(CSystemLog* pLog);
	int start();

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"AsyncClient"; }
	void stop();

	void CreateSendTimer(SendListParams&& aParams);
	void CreateJobTimer(JobListParams&& aParams);
	void FinishedJob(Base::JobPtr&& aJob, std::string aResponse, Base::JobStatus);

	//void MainCircle(SendListParams&& params);
	
	void ProcessSendList(const SendListParams& params);
	void ProcessJobList(const JobListParams& params);

	// handlers
	void VXML2AC_sendCommentEventHandler(const CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void ANY2AC_asyncHttpRequestEventHandler(const CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void ANY2AC_getResultEventHandler(const CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void ConnectHandlers();

	// messages
	void SendAsyncHttpRequestAck(
		const Base::RequestId& aRequestId, 
		const Base::DestinationId aDestinationId,
		const std::wstring& aErrorString,
		bool aIsError);

	void SendAsyncHttpResponse(
		const Base::RequestId& aRequestId, 
		const Base::DestinationId aDestinationId, 
		const Base::JobStatus aJobStatus, 
		const std::string& aResponse,
		const std::wstring& aErrorReason);

private:
	CSystemLog * m_log;
	boost::asio::io_service& r_io;

	CSendListDispatcher mSendDispatcher;
	CJobListDispatcher mJobDispatcher;

	//using MessageMapping = std::map<std::wstring, ChatTextMessageHandler>;

	//MessageMapping _handlers;
};


/******************************* eof *************************************/